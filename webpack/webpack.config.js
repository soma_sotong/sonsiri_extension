const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: {
    main: path.resolve(__dirname, '..', 'application', 'content-scripts', 'main.js'),
    init_script: path.resolve(__dirname, '..', 'application', 'content-scripts', 'init-scripts', 'init-script.js'),
    background: path.resolve(__dirname, '..', 'application', 'background', 'background.js'),
  },
  output: {
    path: path.resolve(__dirname, '..', 'extension', 'build'),
    filename: 'app.[name].js',
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.scss', '.json'],
  },
  module: {
    loaders: [
      {
        test: /\.js$|\.jsx$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
      {
        test: /\.less$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!less-loader'),
      },
      {
        test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        loader: 'file-loader',
      },
    ],
  },
  plugins: [
    new ExtractTextPlugin('[name].css'),
  ],
};
