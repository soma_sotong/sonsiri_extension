import * as ChromeAPI from '../common/util/ChromeAPI'


function setAllRead() {
  chrome.browserAction.setBadgeBackgroundColor({color: [0, 255, 0, 128]});
  chrome.browserAction.setBadgeText({text: ' '});   // <-- set text to '' to remove the badge
}

function setUnread(unreadItemCount) {
  chrome.browserAction.setBadgeBackgroundColor({color: [255, 0, 0, 128]});
  chrome.browserAction.setBadgeText({text: '' + unreadItemCount});
}


// 설치 시 호출되는 리스너
chrome.runtime.onInstalled.addListener((reason) => {
  if (reason) {
    if (reason.reason === 'install') {
      // 처음 설치되었을 때

    }
  }
});

// 익스텐션 아이콘을 클릭했을 때 호출되는 리스너
chrome.browserAction.onClicked.addListener((tab) => {
  // 익스텐션 아이콘을 변경시키는 명령
  // chrome.browserAction.setIcon({ path: 'images/broswer_action/logo_white.png' });

  chrome.tabs.query({ status: 'complete' }, (tabs) => {
    for (let i = 0; i < tabs.length; ++i) {
      const tabId = tabs[i].id;
      const data = {
        msg: ChromeAPI.MSG.APP_SWITCH_TOGGLE,
      };
      ChromeAPI.sendMessageToTab(tabId, { data });
    }
  });
});

// contents-script 에서 전송되는 메시지를 처리하는 리스너
chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.hasOwnProperty('data')) {
    console.log('### message request:', request);
    // send data to server
    switch (request.data.msg) {
      // 모든 탭의 익스텐션을 초기화하라는 요청
      case 'INIT_EXTENTION_IN_ALL_TAB':
        // 크롬 탭 조회를 진행한다.
        chrome.tabs.query({ status: 'complete' }, (tabs) => {
          for (let i = 0; i < tabs.length; ++i) {
            const tabId = tabs[i].id;
            const data = {
              msg: ChromeAPI.MSG.INIT,
            };
            ChromeAPI.sendMessageToTab(tabId, { data });
          }
        });
        break;
      default:
        break;
    }
  }
});

// gcm 메시지 리스너
chrome.gcm.onMessage.addListener((message) => {
  if (message.data) {

  }
});