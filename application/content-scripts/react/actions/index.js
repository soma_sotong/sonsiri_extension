import request from 'axios';

export const RECEIVE_DASHBOARD = 'RECEIVE_DASHBOARD';
export const RECEIVE_KEYWORD = 'RECEIVE_KEYWORD';

export const FETCH_APP_SWITCH = 'FETCH_APP_SWITCH';
export const TOGGLE_APP_SWITCH = 'TOGGLE_APP_SWITCH';

export const CRAWLING = 'CRAWLING';
export const RECOMMENDATION = "RECOMMENDATION";

const API_URL = 'http://localhost:8080';


export function signup(email, companyName) {
  const params = {
    email: email,
    companyName: companyName
  };
  return {
    type: USER_SIGNUP,
    promise: request.post(API_URL + '/api/signup', params)
  }
}


export function fetchAppSwitch(flag) {
  return {
    type: FETCH_APP_SWITCH,
    data: {
      flag,
    }
  }
}

export function toggleAppSwitch() {
  return {
    type: TOGGLE_APP_SWITCH,
  }
}

export function fetchKeyword(keyword) {
  return {
    type: RECEIVE_KEYWORD,
    promise: request.get(API_URL + `/api/keyword?keyword=${keyword}`),
  }
}

export function fetchWebsite(title, thumbnail) {
  return {
    type: CRAWLING,
    data: {
      title,
      thumbnail,
    }
  }
}

export function fetchRecommendation(websites) {
  return {
    type: RECOMMENDATION,
    data: {
      websites,
    }
  }
}
