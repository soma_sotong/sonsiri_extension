/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule user
 */

import {
  CRAWLING,
  RECOMMENDATION,
} from '../actions';

const initialState = {
  title: 'Loading..',
  thumbnail: 'https://t1.daumcdn.net/thumb/R1280x0/?fname=http://t1.daumcdn.net/brunch/service/user/1gtW/image/K7C4zO_PO2l0Vi08eA3PMBgGZ74.JPG',
  recommendation: [],
};

export default function website(state = initialState, action) {
  switch (action.type) {
    case CRAWLING:
      return Object.assign({}, state, action.data);
    case RECOMMENDATION:
      return Object.assign({}, state, { recommendation: action.data.websites });
    default:
      return state;
  }
}