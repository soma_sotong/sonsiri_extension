/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule user
 */

import {
  RECEIVE_DASHBOARD,
  RECEIVE_KEYWORD,
} from '../actions';

const initialState = {
	keywordCount: 65232,
	queueCount: 70927,
	processCount: 1,
	recentKeywords: ['astrologie', 'selbstbewusstsein', 'inneres-wachstum', 'la-marato'],
	dailyKeywordCount: [3000, 27000, 4200, 38000, 2500, 3200, 7600, 4500, 2000, 1580],
	searchKeyword: null,
};

export default function dashboard(state = initialState, action) {
	switch (action.type) {
		case RECEIVE_DASHBOARD:
			return Object.assign({}, state, action.res.data);
		case RECEIVE_KEYWORD:
      return Object.assign({}, state, { searchKeyword : action.res.data });
		default:
  		return state;
	}
}