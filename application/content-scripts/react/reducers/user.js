/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule user
 */

import {
  FETCH_APP_SWITCH,
  TOGGLE_APP_SWITCH,
} from '../actions';

const initialState = {
  appSwitch: false,
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case FETCH_APP_SWITCH:
      return Object.assign({}, state, { appSwitch: action.data.flag });
    case TOGGLE_APP_SWITCH:
      return Object.assign({}, state, { appSwitch: !state.appSwitch });
    default:
      return state;
  }
}