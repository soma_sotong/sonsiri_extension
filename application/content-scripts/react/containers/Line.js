/**
 * Created by hyonzin on 2017. 2. 14. Valentine's day..
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import withinviewport from 'withinviewport';
import * as ChromeAPI from '../../../common/util/ChromeAPI';
import Timer from '../../../common/util/Timer';

const shallowCompare = require('react-addons-shallow-compare');

const MousePosition = { // Enum (top,middle,bottom / left,center,right)
    TopLeft: 0, TopCenter: 1, TopRight: 2,
    MidLeft: 3, MidCenter: 4, MidRight: 5,
    BotLeft: 6, BotCenter: 7, BotRight: 8,
}

const MouseDirection = {
    TopLeft : 0, TopRight: 1,
    BotLeft : 2, BotRight: 3,
}

const linkTagNames = ['a', 'button', 'input', 'select', 'textarea'],
    linkClassNames = [
        ['_1mf', '_1mj'], // Facebook: comment input box
		    ['_3u17'/*, '_3_fz'*/], // Facebook: 'x' button on writing post (role:button)
        ['_1osb'], // Facebook: comment input box (2)
        ['zA'], // Gmail: mail element
        ['kv'], // Gmail: listitem in sequential mail (role:listitem)
        ['kQ'], // Gmail: listitem in sequential mail (2) (role:listitem)
        ['l-t-T-j'], // Google Drive: item
    ];

const timerCollectLinks = new Timer("collectLinks", false),
      timerGetNearest = new Timer("getNearest", false);

class Line extends Component {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const { dispatch } = this.props;
    }

    componentDidMount() {

        let svg = newSVG(),
            bLinkTracking = true,
            nearest = null,
            prevNearest = null,
            line = null,
            rect = null,
            prevClickMX = null,
            prevClickMY = null,
            iLinkTrackingClickRange = getLinkTrackingClickRange(10), // 10px
            links = [],
            iCollectLinksInterval = 1000, // 1000ms
            bCollectLinksFlag = true;

        let curMouseDirection = 0,
            curMouseDirectionVertical = 0,
            curMouseDirectionHorizontal = 0;

        let prevMX = 0,
            prevMY = 0;


        // load bLinkTracking from local storage
        chrome.storage.sync.get('bLinkTracking', function(items) {
            bLinkTracking = items.bLinkTracking;
            if (bLinkTracking == null) {
                bLinkTracking = true;
            }

            if (bLinkTracking) {
              appendSVG();
            }

            setEventListeners (document);
        });

        function getMousePosition(mx, my, rx, ry, rw, rh) {
            let position = (my > ry + rh) ? 6 :
                           (my >= ry) ? 3 : 0;
            position += (mx > rx + rw) ? 2 :
                        (mx >= rx) ? 1 : 0;
            return position;
        }

        function newRect(bound) {
          return createRect({
            'x': bound.left,
            'y': bound.top,
            'width': bound.width,
            'height': bound.height,
            'rx': 3,
            'ry': 3,
            'fill': "rgba(81,132,175,.1)",
            'stroke': "#5184AF",
            'stroke-width': 2,
          });
        }

        function createRect(style) {
            let aRect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
            Object.keys(style).forEach((key)=>{
              aRect.setAttribute(key, style[key]);
            })
            return aRect;
        }

        function newLine(mx, my, bound) {
            // calculate ex, ey according to position
            let borderPoint = getBorderPoint(mx, my, bound);
            return createLine({
              'x1': mx,
              'y1': my,
              'x2': borderPoint.ex,
              'y2': borderPoint.ey,
              'stroke': "#5184AF",
              'stroke-width': 2,
              'stroke-linecap': "round",
              'stroke-dasharray': "1, 5",
            });
        }

        function createLine(style) {
            let aLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
            Object.keys(style).forEach((key)=>{
              aLine.setAttribute(key, style[key]);
            })
            return aLine;
        }

        function getBorderPoint(mx, my, bound) {
            let rx = bound.left,
                ry = bound.top,
                rw = bound.width,
                rh = bound.height;

            // get position
            let position = getMousePosition(mx, my, rx, ry, rw, rh),
                ex, ey;

            switch (position) {
                case MousePosition.TopLeft:
                    ex = rx; ey = ry;
                    break;
                case MousePosition.TopCenter:
                    ex = mx; ey = ry;
                    break;
                case MousePosition.TopRight:
                    ex = rx + rw; ey = ry;
                    break;
                case MousePosition.MidLeft:
                    ex = rx; ey = my;
                    break;
                case MousePosition.MidCenter:
                    ex = mx; ey = my;
                    break;
                case MousePosition.MidRight:
                    ex = rx + rw; ey = my;
                    break;
                case MousePosition.BotLeft:
                    ex = rx; ey = ry + rh;
                    break;
                case MousePosition.BotCenter:
                    ex = mx; ey = ry + rh;
                    break;
                case MousePosition.BotRight:
                    ex = rx + rw; ey = ry + rh;
                    break;
            }

            return { ex: ex, ey: ey };
        }

        function toggleLineTracking() {
            bLinkTracking = !bLinkTracking;
            (bLinkTracking) ? appendSVG() : removeSVG();

            chrome.storage.sync.set({
                'bLinkTracking': bLinkTracking
            });
        }

        function removeLineAndRect() {
            if (line != null) {
                removeFromSVG(line);
                line = null;
            }
            if (rect != null) {
                removeFromSVG(rect);
                rect = null;
            }
            nearest = null;
            prevNearest = null;
        }

        function getLinkTrackingClickRange(pxRange) {
            return Math.pow(pxRange, 2); // to not calculate Square when get distance
        }

        function isVisible(element) {
          let bound = element.getBoundingClientRect();
          return (
              bound.width  > 0
              && bound.height > 0
              && withinviewport(element, "top bottom"))?
            true : false;
        }

        function isMouseOnNearest (mx, my, bound) {
          return (
            getMousePosition(mx, my, bound.left, bound.top, bound.width, bound.height)
              == MousePosition.MidCenter
          )
        }

        function isOutOfRange (mx, my, prevClickMX, prevClickMY) {
          return ((Math.pow(mx - prevClickMX, 2) + Math.pow(my - prevClickMY, 2)) > iLinkTrackingClickRange)?
            true : false;
        }

        function isOnDirection(mx, my, md, linkBound) {

          //let curMouseDirection = getMouseDerection(mx, my, prevMX, prevMY),
          let borderPoint = getBorderPoint(mx, my, linkBound);

          switch (md) {
            case MouseDirection.TopLeft:
              if (borderPoint.ex <= mx && borderPoint.ey <= my) return true;
              break;
            case MouseDirection.TopRight:
              if (borderPoint.ex >= mx && borderPoint.ey <= my) return true;
              break;
            case MouseDirection.BotLeft:
              if (borderPoint.ex <= mx && borderPoint.ey >= my) return true;
              break;
            case MouseDirection.BotRight:
              if (borderPoint.ex >= mx && borderPoint.ey >= my) return true;
              break;
          }
          return false;
        }

        function isDragging () {
            return (!document.getSelection().isCollapsed);
        }

        function isAtTheTop (element) {
            return isDescendant(element, getTopElement(element));
        }

        function isDescendant(parent, child) {
            while (child != null) {
                 if (child == parent) {
                     return true;
                 }
                 child = child.parentNode;
            }

            return false;
        }

        function getTopElement (element) {
            let bound = element.getBoundingClientRect();
            return document.elementFromPoint(bound.left + bound.width/2,
                                             bound.top  + 3); // if set it to "bound.top + bound.height/2",
                                                              // problem occurs when link is more than 1-line.
        }

        function getMouseDerection(mx, my, prevMX, prevMY) {

          curMouseDirectionHorizontal = (mx < prevMX)? 0 : (mx > prevMX)? 1 : curMouseDirectionHorizontal;
          curMouseDirectionVertical = (my < prevMY)? 0 : (my > prevMY)? 2 : curMouseDirectionVertical;

          return curMouseDirectionVertical+curMouseDirectionHorizontal;
        }

        function newSVG() {
          let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
          svg.setAttribute('style', 'position:fixed; top:0; left:0; height:100%; width:100%; pointer-events: none; z-index:1000;');
          svg.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");

          return svg;
        }

        function appendSVG() {
          if (svg == null) return;
          document.getElementById("sotong-line-container").appendChild(svg);
        }

        function removeSVG() {
          if (svg == null) return;
          document.getElementById("sotong-line-container").removeChild(svg);
        }

        function appendToSVG(el) {
          if (svg == null || el == null) return;
          svg.appendChild(el);
        }

        function removeFromSVG(el) {
          if (svg == null || el == null) return;
          try {
            svg.removeChild(el);
          } catch (e) {
            console.log("svg doen't have "+ el)
          }
        }

        function collectLinks(links) {

            if (!bCollectLinksFlag) {
                return links;
            }

            timerCollectLinks.timeStart();

            bCollectLinksFlag = false;

            links = [];
            for (let tagName of linkTagNames) {

                links = links.concat(Array.prototype.slice.call(
                    document.getElementsByTagName(tagName)
                ));
            }

            for (let className of linkClassNames) {

                links = links.concat(Array.prototype.slice.call(
                    document.getElementsByClassName(className.join(' '))
                ));
            }

            setTimeout(() => {
                bCollectLinksFlag = true;
            }, iCollectLinksInterval);


            timerCollectLinks.timeEnd();

            return links;
        }

        function getNearest(mx, my, md, links) {

            timerGetNearest.timeStart();

            let newNearest = null,
                minDist = 1000000,
                bNoLinkWithinViewport = true;


            for (let i = 0; i < links.length; i++) {
                let link = links[i],
                    linkBound = link.getBoundingClientRect();
                if (!isOnDirection(mx, my, md, linkBound)
                 || !isVisible(link)
                 || !isAtTheTop(link)) continue;
                bNoLinkWithinViewport = false;

                let borderPoint = getBorderPoint(mx, my, linkBound),
                    dist = Math.pow(my - borderPoint.ey, 2)
                         + Math.pow(mx - borderPoint.ex, 2);

                if (dist < minDist) {
                    [minDist, newNearest] = [dist, link];
                }
            }

            if (bNoLinkWithinViewport) {
                return null;
            }

            timerGetNearest.timeEnd();

            return newNearest;
        }

        function onMouseMove(e) {
            if (!bLinkTracking) return;

            let mx = e.clientX,
                my = e.clientY,
                nearestBound = null,
                bNoLinkWithinViewport = true;

            curMouseDirection = getMouseDerection(mx, my, prevMX, prevMY);
            prevMX = mx;
            prevMY = my;

            links = collectLinks(links);
            nearest = getNearest(mx, my, curMouseDirection, links);

            if (nearest == null) {
                removeLineAndRect();
                return;
            }

            nearestBound = nearest.getBoundingClientRect();

            if (nearest != prevNearest) {
                removeFromSVG(rect);
                prevNearest = nearest;

                // draw rect
                rect = newRect(nearestBound);
                appendToSVG(rect);
            }

            removeFromSVG(line);
            line = newLine(mx, my, nearestBound);
            appendToSVG(line);
        }

        function setEventListeners (document) {

            document.onmousemove = onMouseMove;

            document.onmousedown = (e) => {
                prevClickMX = e.clientX, prevClickMY = e.clientY;
            }

            document.onclick = (e) => {
                if (!bLinkTracking) return true;

                let mx = e.clientX, my = e.clientY,
                    nearestBound = (nearest != null)?
                    nearest.getBoundingClientRect():null;

                if (nearest == null
                    || isDragging()
                    || isMouseOnNearest(mx, my, nearestBound)
                    || isOutOfRange(mx, my, prevClickMX, prevClickMY)) {

                    return true;
                }
                nearest.click();
                nearest.focus();
            }

            document.onkeyup = (e) => {
                if (e.keyCode == 18) { // Alt on Window, Option on mac
                    toggleLineTracking();
                }
            }

            document.onscroll = () => {
                removeLineAndRect();
            }

            document.onmouseout = (e) => {
                e = e ? e : window.event;
                let target = e.relatedTarget || e.toElement;
                if (!target || target.nodeName == "BODY") {
                    removeLineAndRect();
                }
            }

            document.getElementById("sotong-main-frame").onmouseover = () => {
                removeLineAndRect();
            }

            document.onmouseout = (e) => {
                removeLineAndRect();
            };
        }

    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState);
    }

    render() {
        const { dispatch } = this.props;
        return (
            <div id = "sotong-line-container" > </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        website: state.website,
    };
}

export default connect(mapStateToProps)(Line);
