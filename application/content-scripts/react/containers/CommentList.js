/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule App
 */

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { fetchWebsite } from '../actions';

const shallowCompare = require('react-addons-shallow-compare');

import {Row, Col} from '../template/components/ui/Layout';

import CommentItem from '../components/CommentItem';

class CommentList extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const { dispatch } = this.props;
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const {  } = this.props;

    return (
      <div>
        <Row>
          <div style={{padding:'20px', paddingTop: '10px', marginTop: '25px'}}>
            <CommentItem
              thumbnail="http://admin.lightsinthesky.io/dist/images/5.png"
              name="윤승원"
              count={5}
              text="임씨는 현지 중국 언론 서부망 등에 따르면 왕 부장은 이날 양자회담을 가진 뒤 공동회견에서 올해는 중일 외교 정상화 45주년이며 평화우호조약을 체결한 지 40주년되는 해라면서 양측은 과거 긍정적이거나 부정적이었던 교훈을 통해 중·일 관계를 올바른 방향으로 이끌기위한 노력을 해야한다고 전했다." />
            <CommentItem
              thumbnail="http://admin.lightsinthesky.io/dist/images/img3.jpg"
              name="abcd"
              text="ok"
              count={2}
              reply
            />
            <CommentItem
              thumbnail="http://admin.lightsinthesky.io/dist/images/img3.jpg"
              name="abcd"
              count={0}
              text="hello"
            />
            <CommentItem
              thumbnail="http://admin.lightsinthesky.io/dist/images/5.png"
              name="윤승원"
              text="임씨는 현지 중국 언론 서부망 등에 따르면 왕 부장은 이날 양자회담을 가진 뒤 공동회견에서 올해는 중일 외교 정상화 45주년이며 평화우호조약을 체결한 지 40주년되는 해라면서 양측은 과거 긍정적이거나 부정적이었던 교훈을 통해 중·일 관계를 올바른 방향으로 이끌기위한 노력을 해야한다고 전했다." />
          </div>
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    website: state.website,
  };
}

export default connect(mapStateToProps)(CommentList);