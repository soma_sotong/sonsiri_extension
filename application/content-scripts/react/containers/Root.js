/**
 * Created by bowbowbow on 2017. 2. 13..
 */

// Common modules
import React, { Component } from 'react';
import { Provider } from 'react-redux';

// Own modules
import configureStore from '../store/configureStore';

import App from './App';
import Line from './Line';

const store = configureStore();

export default class Root extends Component {
  render () {
    return (
      <Provider store={store}>
        <div>
          <App/>
          <Line/>
        </div>
      </Provider>
    )
  }
}
