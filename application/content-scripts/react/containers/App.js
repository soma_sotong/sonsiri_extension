import React, { Component, PropTypes } from 'react';
import Superagent from 'superagent';
import { connect } from 'react-redux';
import _ from 'lodash';
import $ from 'jquery';

import Crawler from '../../../common/util/Crawler';

import { fetchRecommendation, fetchAppSwitch, toggleAppSwitch } from '../actions';
import clientListener from '../../init-scripts/clientListener';
import  { fetchInitData }  from '../../init-scripts/fetchInitData';


import WebCard from './WebCard';

// Template modules
import { I, Panel, Button } from '../template/components/ui/';
import { Row, Col, Page } from '../template/components/ui/Layout';

import CommentInput from '../components/CommentInput';
import * as ChromeAPI from '../../../common/util/ChromeAPI';
import Recommend from '../../../common/util/Recommend';

const shallowCompare = require('react-addons-shallow-compare');

import CommentList from './CommentList';

class App extends Component {
  constructor(props) {
    super(props);

    this.handleOnClose = this.handleOnClose.bind(this);
    this.renderWebCard = this.renderWebCard.bind(this);
  }

  componentWillMount() {
    const { dispatch } = this.props;

    clientListener(dispatch);
    fetchInitData(dispatch);

    function isCommandOrCtrl(e) {
      if (e.keyCode === 17 || e.keyCode === 91 || e.keyCode === 93) {
        return true;
      } else {
        return false;
      }
    }

    let dblCtrlKey = false;
    $(document).keyup((e) => {
      if (dblCtrlKey != 0 && isCommandOrCtrl(e)) {
        dispatch(toggleAppSwitch());
      } else if (isCommandOrCtrl(e)) {
        dblCtrlKey = true;
        setTimeout(() => {
          dblCtrlKey = false;
        }, 500);
      }
      if (e.keyCode === 27) {
        dispatch(fetchAppSwitch(false));
      }
    });
  }

  componentDidMount() {
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  renderWebCard(value, key) {
    return (
      <WebCard
        key={key}
        title={value.title}
        thumbnail={value.thumbnail}
        url={value.url}
        description={value.description}
        visitCount={value.visitCount}
        updatedAt={value.updatedAt}
        dispatch={this.props.dispatch}
      />
    )
  }

  handleOnClose() {
    const { dispatch } = this.props;
    dispatch(fetchAppSwitch(false));
  }

  render() {
    const { dispatch, website, user } = this.props;

    if (user.appSwitch === false) {
      return null;
    }

    return (
      <div id="sotong-shortcut-container">
        <Row>
          <div className="webcard-wrapper">
            {
              _.map(website.recommendation, this.renderWebCard)
            }
            {website.recommendation.length === 0 ?
              <WebCard
                title={"아직 데이터가 쌓이지 않았습니다."}
                thumbnail={""}
                url={""}
                description=""
              /> : null
            }
          </div>
          <div
            className="frame-close-button"
            onClick={this.handleOnClose}
          >
          </div>
          {/*
           <div style={{
           backgroundColor: 'white',
           border: '1px solid #e7e7e7',
           bottom: '0px',
           fontSize: '13px',
           marginTop: '3px',
           lineHeight: '30px',
           height: '30px',
           paddingLeft: '7px',
           overflow: 'hidden',
           }}> 공지 영역
           </div>
           */}
        </Row>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    website: state.website,
  };
}

export default connect(mapStateToProps)(App);
