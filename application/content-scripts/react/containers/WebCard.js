/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule App
 */

import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';

import { fetchWebsite } from '../actions';
const shallowCompare = require('react-addons-shallow-compare');
import { Row, Col } from '../template/components/ui/Layout';

import * as ChromeAPI from '../../../common/util/ChromeAPI';
import Recommend from '../../../common/util/Recommend';
import { fetchRecommendation } from '../actions';

const defaultImg = chrome.extension.getURL('../images/app_icon.png')

class WebCard extends Component {
  constructor(props) {
    super(props);

    this.handleOnClick = this.handleOnClick.bind(this);
    this.handleOnClickOpen = this.handleOnClickOpen.bind(this);
    this.handleOnClickRemove = this.handleOnClickRemove.bind(this);

    this.renderButton = this.renderButton.bind(this);
    this.renderTag = this.renderTags.bind(this);
  }

  componentWillMount() {
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  handleOnClick(url) {
    if (url !== "" && url.length > 0) {
      location.assign(url);
    }
  }

  handleOnClickOpen(url) {
    if (url !== "" && url.length > 0) {
      window.open(url, '_blank');
    }
  }

  handleOnClickRemove(url) {
    const { dispatch } = this.props;
    if (url !== "" && url.length > 0) {

      ChromeAPI.getDataFromStorage('history')
      .then((res) => {
        const history = res.history;
        delete history[url];

        ChromeAPI.setDataToStorage({ history })
        .then(() => {
          console.log('finish : setDataToStorage');

          // FIXME : 여기서 website 반환이 안됨, url이 파라미터 미 제거 상태라서 그런가?
          new Recommend(url).getRecommendation().then((websites) => {
            console.log('handleOnClickRemove websites :', websites);
            dispatch(fetchRecommendation(websites));
          });
        });
      });
    }
  }

  renderButton() {
    const { url } = this.props;
    return (
      <div>
        <div
          style={{
            position: 'absolute',
            right: '5px',
            bottom: '5px',
            zIndex: 1000,
          }}
          onClick={() => this.handleOnClickOpen(url)}
        >
          새창
        </div>
        <div
          style={{
            position: 'absolute',
            right: '25px',
            bottom: '5px',
            zIndex: 1000,
          }}
          onClick={() => this.handleOnClickRemove(url)}
        >
          삭제 /
        </div>
      </div>
    )
  }

  renderTags(tag, key) {
    return (
      <div
        key={key}
        style={{
          fontSize: '9px',
          display: 'inline-block',
          backgroundColor: '#EEE',
          borderRadius: '10px',
          width: 'auto',
          color: 'black',
          marginTop: '6px',
          height: '17px',
          padding: '4px',
          overflow: 'hidden',
          marginRight: '3px',
        }}
      >
        {`${tag}`}
      </div>
    )
  }

  render() {
    const { title, thumbnail, url, description, visitCount, updatedAt } = this.props;

    // 태그 생성
    const tags = [];
    if (visitCount > 5) {
      tags.push(Math.floor(visitCount / 5 + 1) * 5 + `+`);
    } else if (visitCount > 0) {
      tags.push(`${visitCount}번`);
    }

    let tUrl = _.truncate(url, {
      length: 60,
      omission: '...',
    });

    const updatedDate = new Date(updatedAt);
    if (updatedDate.getTime() > new Date().getTime() - 60 * 1000 * 60 * 24) {
      tags.push('오늘');
    } else if (updatedDate.getTime() > new Date().getTime() - 60 * 1000 * 60 * 24 * 2) {
      tags.push('어제');
    }

    let tDescription = _.truncate(description, {
      length: 60,
      omission: '...',
    });

    let tTitle = _.truncate(title, {
      length: 40,
      omission: '...',
    });

    let tThumbnail = thumbnail;
    if (tThumbnail === "") {
      tThumbnail = defaultImg;
    }

    return (
      <div
        className="webcard-item"
        style={{ borderBottom: '1px solid #EEE', padding: '10px', position: 'relative' }}
      >
        <Row>
          {url !== "" ? this.renderButton() : null}
          <Col size={2}>
            <img
              style={{
                width: '40px',
                height: '40px',
                objectFit: 'cover',
                borderRadius: '4px',
                border: '1px solid #EEE'
              }}
              src={tThumbnail}
            />
          </Col>
          <Col size={10} >
            <div style={{ paddingTop: '1px' }}>
              <div style={{
                fontSize: '13px',
                fontWeight: '600',
                marginTop: '3px',
                height: '17px',
                overflow: 'hidden'
              }}
                   onClick={() => this.handleOnClick(url)}
              > {tTitle}

              </div>

              {(tDescription && tDescription !== "") ?
                (<div style={{
                  fontSize: '10px',
                  marginTop: '3px',
                  height: '13px',
                  overflow: 'hidden'
                }}> {tDescription} </div>)
                : null }

              <div
                style={{
                  fontSize: '9px',
                  color: 'rgb(38, 38, 38)',
                  marginTop: '2px',
                  height: '10px',
                  overflow: 'hidden'
                }}
              >
                {tUrl}
              </div>
              { url !== "" ?
                <div>
                  {
                    _.map(tags, this.renderTag)
                  }
                </div> : null
              }
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

WebCard.propTypes = {
  thumbnail: React.PropTypes.string,
  title: React.PropTypes.string,
  url: React.PropTypes.string,
  description: React.PropTypes.string,
  visitCount: React.PropTypes.number,
  dispatch: React.PropTypes.func,
};

WebCard.defaultProps = {
  thumbnail: "",
  title: "Loading",
  url: "Loading",
  description: "Loading",
  visitCount: 0,
  updatedAt: 0,
};

export default WebCard;
