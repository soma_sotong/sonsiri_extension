/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule App
 */

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash'

import { fetchWebsite } from '../actions';
import CommentInput from './/CommentInput';

const shallowCompare = require('react-addons-shallow-compare');


class CommentItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      more: false,
      updown: 0,
      myCount: this.props.count,
      replyInput: false,
    };

    this.renderTextArea = this.renderTextArea.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.handleUpDown = this.handleUpDown.bind(this);
  }

  componentWillMount() {
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  handleUpDown(value) {
    const { updown } = this.state;
    if (value !== 1 && value !== -1) {
      return;
    }

    if (updown === value) {
      this.setState({
        updown: 0,
        myCount: this.props.count,
      })
    } else {
      this.setState({
        updown: value,
        myCount: this.props.count + value,
      })
    }
  }

  renderTextArea() {
    const { more } = this.state;
    const { text } = this.props;

    return (
      <div style={{ fontSize: '12px', paddingTop: '17px' }}>
        {
          more ? text :
            _.truncate(text, {
              length: 80,
              omission: '...',
            })
        }
        {text.length >= 80 ?
          <span
            className="text-info"
            style={{ fontSize: '11px', paddingLeft: '4px', cursor: 'pointer' }}
            onClick={ () => {
              this.setState({ more: !more })
            } }
          >
          {more ? 'fold' : 'more'}
          </span> : null}
      </div>
    );
  }

  renderFooter() {
    const { reply } = this.props;
    const { updown, myCount, replyInput } = this.state;

    return (
      <div style={{ paddingTop: '5px', color: '#414141', height: '10px' }}>
        {!reply ?
          <div
            style={{ cursor: 'pointer', display: 'inline-block', textDecoration: 'underline' }}
            onClick={() => this.setState({ replyInput: !replyInput })}
          >reply </div>
          : null
        }
        <div className="updown">
          <div className="count">{myCount}</div>
          <div className={`up ${(updown === 1 ? 'selected' : '')}`} onClick={() => this.handleUpDown(1)}></div>
          <div className={`down ${(updown === -1 ? 'selected' : '')}`} onClick={() => this.handleUpDown(-1)}></div>
        </div>
      </div>
    )
  }

  render() {
    const { thumbnail, name, text, reply } = this.props;
    const { replyInput } = this.state;

    let commentClass = 'comment-item';
    if (reply) {
      commentClass = 'comment-item-reply';
    }
    return (
      <div>
        <article className={commentClass}>
          <a className="pull-left thumb-sm m-r-sm">
            <img src={thumbnail} className="img-circle" style={{ width: '30px', height: '30px', objectFit: 'cover' }}/>
          </a>
          <section className="comment-body"
                   style={{ paddingTop: '7px', borderBottom: '1px solid #EEE', paddingBottom: '14px' }}
          >
            <header>
              <a href="#"><strong style={{ fontSize: '13px' }}>{name} </strong></a>
              <span className="pull-right">5분 전</span>
            </header>
            {this.renderTextArea()}
            {this.renderFooter()}
          </section>
          {replyInput? <CommentInput/>:null}
        </article>
      </div>
    );
  }
}

CommentItem.propTypes = {
  thumbnail: React.PropTypes.string,
  name: React.PropTypes.string,
  text: React.PropTypes.string,
  reply: React.PropTypes.bool,
  count: React.PropTypes.number,
};

CommentItem.defaultProps = {
  reply: false,
  count: 0,
};


function mapStateToProps(state) {
  return {
    website: state.website,
  };
}

export default connect(mapStateToProps)(CommentItem);