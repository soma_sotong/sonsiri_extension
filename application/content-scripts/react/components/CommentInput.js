/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * @providesModule App
 */

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash'

import { fetchWebsite } from '../actions';

const shallowCompare = require('react-addons-shallow-compare');


class CommentInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  componentWillMount() {
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    const { reply } = this.props;

    return (
      <div style={{marginTop: '10px'}}>
              <textarea
                className="form-control"
                rows="2"
              />
        <div
          className="btn btn-default pull-right"
          style={{width:'45px', height: '25px', fontSize: '12px', padding:'4px 11px', backgroundColor: 'white', marginTop: '5px'}}
        >작성</div>
      </div>
    );
  }
}

CommentInput.propTypes = {
  thumbnail: React.PropTypes.string,
};

CommentInput.defaultProps = {
  reply: false,
};


function mapStateToProps(state) {
  return {
    website: state.website,
  };
}

export default connect(mapStateToProps)(CommentInput);