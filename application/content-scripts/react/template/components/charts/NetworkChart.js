/**
 * Copyright 2015-present, Lights in the Sky (3273741 NS Ltd.)
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE file in the root directory of this source tree. 
 * 
 * @providesModule BarChart
 */

import React from 'react';

import vis from 'vis';
import uuid from 'uuid';

export default class NetworkChart extends React.Component {
  constructor(props) {
    super(props);
    const {identifier} = this.props;
    this.updateGraph = this.updateGraph.bind(this);
    this.state = {
      hierarchicalLayout: true,
      identifier : identifier !== undefined ? identifier : uuid.v4()
    };
  }

  componentDidMount() {
    this.updateGraph();
  }

  componentDidUpdate() {
    this.updateGraph();
  }

  changeMode(event) {
    this.setState({hierarchicalLayout: !this.state.hierarchicalLayout});
    this.updateGraph();
  }

  updateGraph() {
    let container = document.getElementById(this.state.identifier);
    let options = {
      stabilize: false,
      smoothCurves: false,
      edges: {
        color: '#000000',
        width: 0.5,
        arrowScaleFactor: 0.5,
        style: 'arrow',
        dashes: true,
      },
      nodes: {
        color: '#CCC',
        font: '11px arial #000',
        size: 35,
      }
    };

    if (this.state.hierarchicalLayout) {
      options.hierarchicalLayout = {
        enabled: true,
        direction: 'UD',
        levelSeparation: 100,
        nodeSpacing: 1
      };
    } else {
      options.hierarchicalLayout = {
        enabled: false
      };
    }

    new vis.Network(container, this.props.graph, options);
  }

  render() {
    const {identifier} = this.state;
    const {style} = this.props;
    return React.createElement('div', {onDoubleClick: this.changeMode.bind(this), id: identifier, style}, identifier);
  }
}

NetworkChart.defaultProps = {
  graph: {},
  style: {width: '100%', height: '480px'}
};

