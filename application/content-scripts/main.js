// Common Modules..
require('./main.less');

import React from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';

import { fetchInitData } from './init-scripts/fetchInitData';

import Root from './react/containers/Root';

/*
fetchInitData().then(() => {
  ReactDOM.render(
    <Root/>,
    document.getElementById('sotong-main-frame')
  );
});
*/

ReactDOM.render(
  <Root/>,
  document.getElementById('sotong-main-frame')
);
