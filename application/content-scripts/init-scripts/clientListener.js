import { fetchInitData } from './fetchInitData';
import React from 'react';
import { connect } from 'react-redux';
import * as ChromeAPI from '../../common/util/ChromeAPI';
import { fetchAppSwitch, toggleAppSwitch } from '../react/actions';

import Alert from 'react-s-alert';
// import ActivityAlert from '../../react/addit-widget/components/panels/ActivityAlert.Panel';
import jquery from 'jquery';

/**
 * Listener for messages from background (onRespond from server)
 *
 */

const activityAlertBody = (activity) => (
  <ActivityAlert activity={activity}/>
);

export default function clinetListener(dispatch, props) {
  chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    console.log(':: clinetListener :: request', request);

    // 잘 받았다는 응답 보냄
    sendResponse({ farewell: true });

    if (request.hasOwnProperty('data')) {
      // send data to server
      switch (request.data.msg) {
        case ChromeAPI.MSG.INIT:
          fetchInitData();
          break;
        case ChromeAPI.MSG.APP_SWITCH_TOGGLE:
          dispatch(toggleAppSwitch());
        default:
          break;
      }
    }
  });
}

