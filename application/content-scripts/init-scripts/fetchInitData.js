// Common Modules...
import _ from 'lodash';
import $ from 'jquery';
import Superagent from 'superagent';

import Crawler from '../../common/util/Crawler';
import ImageMiner from '../../common/util/ImageMiner';
import Recommend from '../../common/util/Recommend';
import { fetchRecommendation } from '../react/actions';

// Own Modules...
import * as ChromeAPI from '../../common/util/ChromeAPI';

function getCookie(cName) {
  const ARRcookies = document.cookie.split(';');
  for (let i = 0; i < ARRcookies.length; i++) {
    let x = ARRcookies[i].substr(0, ARRcookies[i].indexOf('='));
    const y = ARRcookies[i].substr(ARRcookies[i].indexOf('=') + 1);
    x = x.replace(/^\s+|\s+$/g, '');
    if (x === cName) {
      return unescape(y);
    }
  }
  return null;
}

function requestTitle(url, parameter) {
  return new Promise((resolve, reject) => {
    if (_.isString(url)) {
      Superagent
      .get(url)
      .send()
      .end((err, res) => {
        if (err) {
          resolve({ err });
        } else {
          const mCrawler = new Crawler(res.text);
          resolve({ title: mCrawler.getTitle(), parameter });
        }
      });
    } else {
      reject(new Error('Parameters are missed'));
    }
  });
}

function parseQueryString() {
  const queryObject = {};
  const query = window.location.search.substring(1);
  const vars = query.split("&");
  for (let i = 0; i < vars.length; i++) {
    const pair = vars[i].split("=");
    // If first entry with this name
    if (typeof queryObject[pair[0]] === "undefined") {
      queryObject[pair[0]] = decodeURIComponent(pair[1]);
      // If second entry with this name
    } else if (typeof queryObject[pair[0]] === "string") {
      const arr = [queryObject[pair[0]], decodeURIComponent(pair[1])];
      queryObject[pair[0]] = arr;
      // If third or later entry with this name
    } else {
      queryObject[pair[0]].push(decodeURIComponent(pair[1]));
    }
  }
  return queryObject;
}

function getMeaningfulParameter() {
  return new Promise((resolve, reject) => {
    const QueryObject = parseQueryString();
    console.log('QueryObject :', QueryObject);

    let urlWithoutParameter = document.URL.replace(window.location.search, '').replace(window.location.hash, '');

    let validOriginalUrl = urlWithoutParameter + '?';
    Object.keys(QueryObject).forEach((key) => {
      const value = QueryObject[key];
      if (value !== "undefined" && value !== "") {
        if (validOriginalUrl.indexOf('=') !== -1) {
          validOriginalUrl += '&';
        }
        validOriginalUrl += `${key}=${value}`;
      }
    });

    const titleRequests = [];
    Object.keys(QueryObject).forEach((key) => {
      const value = QueryObject[key];
      if (value !== "undefined" && value !== "") {
        let tUrl = validOriginalUrl.replace(`&${key}=${value}`, '');
        tUrl = tUrl.replace(`${key}=${value}`, '');
        titleRequests.push(requestTitle(tUrl, `${key}=${value}`));
      }
    });

    let resultUrl = validOriginalUrl;
    requestTitle(validOriginalUrl)
    .then((original) => {
      console.log('originalTitle: ', original.title);

      Promise.all(titleRequests)
      .then((titles) => {

        titles.forEach((value) => {
          if (!value.error && value.title === original.title) {
            resultUrl = resultUrl.replace('&' + value.parameter, '');
            resultUrl = resultUrl.replace(value.parameter, '');
          }
        });

        if (resultUrl[resultUrl.length - 1] === '?') {
          resultUrl = resultUrl.replace('?', '');
        }

        console.log('resultUrl:', resultUrl);
        resolve(resultUrl);
      })
    });
  });
}

function registerHistory(before, now) {
  const pageSource = document.head.outerHTML + document.body.outerHTML;

  const mCrawler = new Crawler(pageSource);
  const mImageMiner = new ImageMiner(mCrawler.getDollar());

  mImageMiner.miningStone();
  let history = {};
  ChromeAPI.getDataFromStorage('history')
  .then((res) => {
    const historyL = res.history;
    if (historyL) {
      history = historyL;
    }
    return mImageMiner.miningGold();
  })
  .then((imgArray) => {
    let visitCount = 1;
    let updatedAt = new Date().getTime();
    if (history[now]) {
      visitCount = history[now].visitCount + 1;
    }

    let img = "";
    if (imgArray.length > 0) {
      img = imgArray[0].img;
    }

    // 마지막에 붙은 파라미터 제거
    if (now.length > 0 && (now[now.length - 1] === '?' || (now[now.length - 1] === '#'))) {
      now = now.substr(0, now.length - 1);
    }

    history[now] = {
      url: now,
      before,
      title: mCrawler.getTitle(),
      description: mCrawler.getDescription(),
      thumbnail: img,
      visitCount,
      updatedAt,
    };
    return ChromeAPI.setDataToStorage({ history });
  })
  .then(() => {

  });
}

function periodicLocationCheck(dispatch) {
  getMeaningfulParameter()
  .then((now) => {
    new Recommend(now).getRecommendation().then((websites) => {
      dispatch(fetchRecommendation(websites));
    });
    ChromeAPI.getDataFromStorage('now').then((res) => {
      // document.referer, now
      registerHistory(res.now, now);
      ChromeAPI.setDataToStorage({ now }).then();
    });
  });

  let before = document.URL;
  // SPA 대응
  setTimeout(() => {
    if (document.URL != before) {
      getMeaningfulParameter()
      .then((now) => {
        ChromeAPI.getDataFromStorage('now').then((res) => {
          // document.referer, now
          registerHistory(res.now, now);
          ChromeAPI.setDataToStorage({ now }).then();
        });
      });
      before = document.URL;
    }
  }, 2000);
}

function fetchInitData(dispatch) {
  return new Promise((resolve, reject) => {
    console.log(':: fetchInitData :: called');

    periodicLocationCheck(dispatch);
    resolve();
  });
}

exports.fetchInitData = fetchInitData;
