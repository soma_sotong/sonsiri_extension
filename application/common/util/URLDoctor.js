import _ from 'lodash';

/**
 * 혼돈의 URL 을 치료해서 정제하는 클래스
 */
export default class URLDoctor {
  /**
   * @param url {string}
   */
  constructor(url) {
    this.url = url;
  }

  getHostname() {
    const a = document.createElement('a');
    a.href = this.url;
    return a.hostname;
  }

  /**
   * relative url을 absolute url 로 변환한다.
   */
  relativeToAbsolute() {
    const url = this.url;
    // 상대주소가 아니면 바로 리턴
    if (url.indexOf('//') !== -1) {
      return url;
    }

    /* 이미지가 상대 경로이면 절대 경로로 변환시킨다. */
    let tempUrl = url;
    const parser = document.createElement('a');
    parser.href = document.URL;

    const protocol = parser.protocol;
    const host = parser.host;

    if (tempUrl && tempUrl.length >= 1 && tempUrl[0] !== '/') {
      tempUrl = `/${tempUrl}`;
    }

    return `${protocol}//${host}${tempUrl}`;
  }

  /**
   * URL 파편화를 위해 표준 URL 을 추출하는 함수
   * 현재 네이버와 구글의 검색화면에서만 구현되어 있음
   * @returns standardURL {string}
   */
  extractStandardURL() {
    let url = this.url;

    const parser = document.createElement('a');
    parser.href = url;

    const query = this.parseQuery(location.search.substr(1));

    // 네이버 검색 meta 쿼리 제거
    if (url.indexOf('search.naver.com') !== -1) {
      // ex> https://search.naver.com/search.naver?where=nexearch&query=%ED%94%BC%EB%B2%97&sm=top_hty&fbm=1&ie=utf8
      url = this.removeHashTag(url, window.location.hash);

      _.forIn(query, (value, key) => {
        if (key !== 'query') {
          url = url.replace(`${key}=${value}`, '');
        }
      });
      url = url.replace(/&/g, '');
    } else if (url.indexOf('google') !== -1 && (url.indexOf('search') !== -1 || url.indexOf('q='))) {
      // ex> https://www.google.co.kr/?gfe_rd=cr&ei=T_njV9u0GqGL8QfxvIO4DQ#newwindow=1&q=apple
      // ex> https://www.google.co.kr/search?q=apple#q=apple&newwindow=1&start=10

      url = this.removeHashTag(url, window.location.hash);
      _.forIn(query, (value, key) => {
        if (key !== 'q') {
          url = url.replace(`${key}=${value}`, '');
        }
      });
      url = url.replace(/&/g, '');
    }
    return url;
  }


  /**
   * query string을 object 에 key, value 형태의 json 으로 파싱 함
   * @param queryString {string}
   * @returns queryJson {json}
   */
  parseQuery(queryString) {
    const query = queryString;
    const result = {};
    query.split('&').forEach((part) => {
      const item = part.split('=');
      result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
  }

  /**
   * 해시 태그 제거
   * @param url {string}
   * @param hashTag {string}
   * @returns url {string}
   */
  removeHashTag(url, hashTag) {
    if (hashTag) {
      return url.replace(hashTag.toString(), '');
    }
    return url;
  }
}
