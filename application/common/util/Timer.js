/*
 * Module for profiling by time
 * usage:
 *  let timer = new Timer("label", true); // works only second param == true
 *  timer.timeStart();
 *  ...
 *  timer.timeEnd();
 */

export default class Timer {

  constructor (label, isDebugging) {
    this.label = label;
    this.isDebugging = isDebugging;
  }

  timeStart () {
    this.isDebugging && console.time(this.label);
  }

  timeEnd () {
    this.isDebugging && console.timeEnd(this.label);
  }

  log () {
    this.isDebugging && console.log(arguments);
  }
}
