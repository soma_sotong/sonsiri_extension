import _ from 'lodash';

export const STORAGE = {
  TURN_ON: 'TURN_ON',
  TURN_OFF: 'TURN_OFF',
};

export const MSG = {
  INIT: 'INIT',
  APP_SWITCH_TOGGLE: 'APP_SWITCH_TOGGLE',
};

/**
 * Stroage가 가지고 있는 정보를 가져온다.
 *
 * @param key {String}
 * @param callback {Function} <Optional>
 */
export function getDataFromStorage(key) {
  return new Promise((resolve, reject) => {
    const result = {};
    chrome.storage.local.get(key, (items) => {
      for (const key in items) {
        const data = items[key].data;
        const timeStamp = items[key].timeStamp;
        const ttl = items[key].ttl;

        if (_.isNumber(ttl) && (timeStamp + ttl < new Date().getTime())) {
          console.log(`key : ${key} timeout`);
          continue;
        }

        if (data) {
          _.set(result, key, data);
        }
      }
      resolve(result);
    });
  });
}

/**
 * Stroage에 정보를 저장한다.
 * @param obj
 * @param option.ttl {number} : time to alive (단위 : 분)
 * @param callback
 */
export function setDataToStorage(obj, option = {}) {
  return new Promise((resolve, reject) => {
    const tObj = {};
    for (const key in obj) {
      if (key) {
        _.set(tObj, [key, 'data'], obj[key]);
        _.set(tObj, [key, 'timeStamp'], new Date().getTime());
        if (_.isNumber(option.ttl)) {
          _.set(tObj, [key, 'ttl'], option.ttl);
        }
      }
    }

    chrome.storage.local.set(tObj, () => {
      resolve();
    });
  });
}

/**
 * Stroage의 모든 데이터를 제거한다.
 *
 * @param callback {Function}
 */
export function clearStorage(callback = () => {
}) {
  chrome.storage.local.clear(callback);
}

/**
 * Tab에 데이터를 보낸다.
 *
 * @param tabId {number} : 전송할 탭아이디.
 * @param data {object} : 전송할 객체
 * @param tryNum {number} : 현재 재 전송횟수
 * @param retryNum {number} : 최대 재 전송횟수
 */
export function sendMessageToTab(tabId, data, tryNum = 0, retryNum = 5) {
  chrome.tabs.sendMessage(tabId, data, (response) => {
    try {
      return response.farewell;
    } catch (err) {
      tryNum++;
      if (tryNum < retryNum) {
        setTimeout(() => {
          return (sendMessageToTab(tabId, data, tryNum++, retryNum));
        }, 1000);
      }
    }
    return -1;
  });
}

/**
 * 백그라운드로 메시지를 전송한다
 * @param data
 */
export function sendMessageToBackground(data) {
  chrome.runtime.sendMessage({
    data,
  });
}



