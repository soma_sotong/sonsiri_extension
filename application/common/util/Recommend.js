import _ from 'lodash';
import * as ChromeAPI from './ChromeAPI';

export default class Recommend {
  constructor(url) {
    this.nowURL = url;
    this.urlMap = new Map();
    this.graph = {
      /**
       * url: {
       *  next: {},
       *  before: {},
       * }
       *
       */
    };
    this.urlMark = new Set();
    this.websites = [];
    this.maxDepth = 4;
  }

  dfs(now, depth) {
    if (depth > this.maxDepth) {
      return;
    }

    if (!this.graph[now]) return;

    // 앞으로
    const next = this.graph[now].next;
    for (const nextURL in next) {
      if (!nextURL) continue;
      if (!this.urlMark.has(nextURL)) {
        const urlObject = this.urlMap.get(nextURL);
        if (urlObject) {
          this.websites.push(this.urlMap.get(nextURL));
          this.urlMark.add(nextURL);
        }
      }
      this.dfs(nextURL, depth + 1);
    }

    // 뒤로
    const before = this.graph[now].before;
    for (const beforeURL in before) {
      if (!beforeURL) continue;
      if (!this.urlMark.has(beforeURL)) {
        const urlObject = this.urlMap.get(beforeURL);
        if (urlObject) {
          this.websites.push(this.urlMap.get(beforeURL));
          this.urlMark.add(beforeURL);
        }
      }
      this.dfs(beforeURL, depth + 1);
    }
  }

  getRecommendation() {
    return new Promise((resolve, reject) => {
      this.urlMark.add(this.nowURL);

      ChromeAPI.getDataFromStorage('history')
      .then((res) => {
        const history = res.history;
        if (history) {

          // 맵에 삽입
          for (const url in history) {
            this.urlMap.set(url, history[url]);
          }

          // 양방향 그래프 구성
          for (const url in history) {
            const before = history[url].before;

            if (this.graph[before]) {
              this.graph[before].next[url] = 1;
            } else {
              this.graph[before] = {
                next: {},
                before: {},
              };
              this.graph[before].next[url] = 1;
            }

            if (this.graph[url]) {
              this.graph[url].before[before] = 1;
            } else {
              this.graph[url] = {
                next: {},
                before: {},
              };
              this.graph[url].before[before] = 1;
            }
          }

          console.log('[recommend] this.graph[this.nowURL]:', this.graph[this.nowURL]);

          if (this.graph[this.nowURL]) {
            this.dfs(this.nowURL, 0);

            this.websites = this.websites.sort((a, b) => {
              return a.visitCount > b.visitCount ? -1 : 1;
            });

            resolve(this.websites);
          }
        }
      });
    });
  }
}
