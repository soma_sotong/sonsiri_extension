import _ from 'lodash';
import URLDoctor from './URLDoctor';

/**
 *
 * 이미지 수집 로직
 *
 * 1. 이미지 수집
 *  이미지 수집 도중 수집한 이미지가 10개를 초과하면 중단한다.
 *  이때 base64형태로 된 이미지는 제외한다.
 *  이미지 순서가 뒤에 있을 수록 가중치를 떨어뜨린다.
 *  모든 이미지는 절대 경로로 변환시킨다.
 *
 *  1-1. head의 meta태그에서 (og:thumbnail, twitter:image) 태그의 이미지를 뽑는다.  (score : 300)
 *  1-2. img 태그의 이미지를 뽑는다. (score: 50 - 순서)
 *  1-3. div 태그의 background-image 속성의 이미지를 뽑는다. (score : 10-순서) -> 미구현
 *
 * 이 단계에서 일단 이미지를 슬라이드에 배치한다.
 *
 * 2. 특정 목적의 이미지에 벌점을 준다.
 *    이미지의 목적을 판단할 때 경로의 이름을 이용한다.
 *    2-1. 프로필 이미지 :  경로에 “profile”이 포함된 이미지 (score -= -50)
 *    2-2. 로딩 이미지 : 경로에 “spin”, “loading”이 포함된 이미지 (score -= 30)
 *    2-3. 아이콘 이미지 : 경로에 “icon”이 포함된 이미지 (score -= 20)
 *
 * 3. 이미지 크기와 비율에 따라 가중치를 부여한다.
 *    이 때 이미지의 비율이 1:1에 가깝고 이미지의 크기가 적당한 크기 일 때 높은 가중치를 부여한다.
 *    ratioWeight = (1-(width/height-1)^2)
 *    sizeWeight = (500px  <= width &&  width <= 1000px )*100 + (1000px  < width &&  width <= 1500px )*60
 *    score += sizeWeight*ratioWeight
 *
 * 4. scroe 순서로 이미지를 정렬한다.
 *
 * 5. 이 10개의 이미지에 대해 유효한지 ajax로 검사해서 유효하지 않은 이미지는 비동기적으로 제외시킨다.
 *
 * 6. 선정된 이미지를 슬라이드에 다시 배치시킨다.
 */

const config = {
  imageCandidateLimit: 30,
};

/**
 * 웹을 대표하는 예쁜 이미지를 추출하는 클래스
 */
export default class ImageMiner {
  /**
   * @param $ {object}
   */
  constructor($) {
    this.$ = $;
    this.imgArray = [];
  }

  getImgArray() {
    return this.imgArray;
  }

  /**
   * 원석을 수집한다.
   * @returns {Array}
   */
  miningStone() {
    this.extractMetaImage();
    this.extractTagImg();
  }

  /**
   * 원석에서 금을 고른다.
   * @param callback
   */
  miningGold() {
    return new Promise((resolve, reject) =>{
      const tasks = [];
      this.imgArray.forEach((v) => {
        tasks.push(this.loadImage(v));
      });

      const imgArray2 = [];
      Promise.all(tasks)
      .then((loadedImages) => {
        loadedImages.forEach((imgEl) => {
          if (imgEl) {
            imgArray2.push(imgEl);
          }
        });
        this.imgArray = imgArray2;

        this.scorinAndSort();
        resolve(this.imgArray);
      });
    });
  }

  scorinAndSort() {
    for (let i = 0; i < this.imgArray.length; i++) {
      const imgEl = this.imgArray[i];
      const imgUrl = imgEl.img;

      // 이름으로 벌점을 준다.
      if (imgUrl.indexOf('profile') !== -1) {
        imgEl.score -= 50;
      }
      if (imgUrl.indexOf('loading') !== -1 || imgUrl.indexOf('profile') !== -1) {
        imgEl.score -= 30;
      }
      if (imgUrl.indexOf('icon') !== -1) {
        imgEl.score -= 20;
      }

      // 비율과 크기로 가중치를 준다.
      const width = imgEl.width;
      const height = imgEl.height;

      if (height > 0) {
        const ratioWeight = (1 - (width / height - 1) * (width / height - 1));
        const sizeWeight = (width >= 500 && width <= 1000) * 200 + (width > 1000 && width <= 1500) * 150;

        imgEl.score += ratioWeight * sizeWeight;
      }

      if (width < 200) {
        imgEl.score -= 50;
      }
    }

    this.imgArray.sort((a, b) => {
      return b.score - a.score;
    });
  }


  /**
   * 이미지를 후보군에 푸시한다.
   * 뒤에 추가되는 이미지 일수록 score 에서 점수를 뺀다.
   * @param img {string}
   * @param score {number}
   */
  imgPush(img, score) {
    // base64 이미지가 아카이브에 저장되면 용량이 매우 커질 수 있기 때문에
    if (img && _.isString(img) && img !== '' && img.indexOf('base') === -1) {
      const imgCount = this.imgArray.length;

      if (imgCount < config.imageCandidateLimit) {
        this.imgArray.push({
          img: new URLDoctor(img).relativeToAbsolute(),
          score: score - imgCount,
        });
      }
    }
  }

  /**
   * 메타 태그에서 이미지를 추출한다.
   * 해당 웹사이트가 제시하는 대표 이미지로 가장 신뢰할 수 있다.
   * 가중치 300을 부여한다.
   */
  extractMetaImage() {
    const ogImage = this.$('meta[property="og:image"]').attr('content');
    if (ogImage) {
      this.imgPush(ogImage, 300);
      return;
    }

    const twitterImage = this.$('meta[name="twitter:image"]').attr('content');
    this.imgPush(twitterImage, 300);
  }

  extractTagImg() {
    const images = this.$('img');
    _.forIn(images, (v) => {
      const imgObj = v.attribs;
      if (imgObj && _.isString(imgObj.src) && imgObj.src !== '') {
        const img = imgObj.src;
        this.imgPush(img, 50);
      }
    });
  }

  loadImage(imgEl) {
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.src = imgEl.img;
      img.onload = function (e) {

        const width = this.width;
        const height = this.height;

        const imgElWithInfo = {
          img: imgEl.img,
          score: imgEl.score,
          width,
          height,
        };

        if ((width / height) > 2.5 || (height / width) > 2.5 || width < 50 || height < 50) {
          resolve();
        } else {
          resolve(imgElWithInfo);
        }
      };
      img.onerror = function () {
        resolve()
      };
    });
  }
}
