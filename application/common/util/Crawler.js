import cheerio from 'cheerio';
import _ from 'lodash';
import URLDoctor from './URLDoctor';
import ImageMiner from './ImageMiner';

export default class Crawler {
  /**
   * @param pageSource {string}
   */
  constructor(pageSource) {
    this.host = {
      pageSource,
    };

    this.$ = cheerio.load(this.host.pageSource);
    this.meta = this.$('meta');
    this.link = this.$('link');
  }

  getDollar() {
    return this.$;
  }

  /**
   * Favicon을 가져온다.
   * chrome background 에서 제공하는 tab 에서
   * 안정적인 favicon을 제공하기 때문에 이 메서드를 사용하지 않음
   */
  getFavicon() {
    const link = this.link;
    let favicon = null;
    const faviconRel = ['shortcut icon', 'icon', 'apple-touch-icon-precomposed'];

    _.forIn(link, (value, key) => {
      if (value.hasOwnProperty('attribs')) {
        const tag = value.attribs;
        if (tag.hasOwnProperty('rel') && (faviconRel.indexOf(tag.rel.toLowerCase()) !== -1)) {
          favicon = tag.href;
        }
      }
    });

    if (favicon && favicon.indexOf('//') === -1) {
      favicon = new URLDoctor(favicon).relativeToAbsolute();
    }
    console.log('[Parasite] favicon url : ', favicon);
    return favicon;
  }

  /**
   * Title을 가져온다.
   */
  getTitle() {
    const tmpMetaData = this.meta;
    let title = null;

    for (const key in tmpMetaData) {
      if (tmpMetaData[key].hasOwnProperty('attribs')) {
        const meta = tmpMetaData[key].attribs;

        if (meta.property === 'og:title') {
          title = meta.content;
          break;
        }

        if (meta.name === 'twitter:title') {
          title = meta.content;
          break;
        }
      }
    }

    if (!title) {
      // meta 태그에는 타이틀이 없지만 <title></title> 형태로 타이틀이 주어진 경우가 있어서
      title = this.$('title').text();
    }

    return _.trim(title);
  }

  /**
   * Description을 추출한다.
   */
  getDescription() {
    const tmpMetaData = this.meta;
    const description = [''];

    for (const key in tmpMetaData) {
      if (tmpMetaData[key].hasOwnProperty('attribs')) {
        const meta = tmpMetaData[key].attribs;

        if (meta.property === 'og:description') {
          description.unshift(meta.content);
        }

        if (meta.name === 'twitter:description' || meta.name === 'description') {
          description.unshift(meta.content);
        }
      }
    }

    return description[0];
  }

  /**
   * url의 소스를 가져온다.
   */
  getPageSource() {
    return this.host.pageSource;
  }
}