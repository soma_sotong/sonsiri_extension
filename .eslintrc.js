module.exports = {
    "extends": "airbnb",
    "installedESLint": true,
    "plugins": [
        "react"
    ],
    "rules": {
        "new-cap": 0,
        "react/jsx-indent-props": 0,
        "react/jsx-indent": 0,
        "max-len": 0,
        'object-curly-spacing': 0,
        "no-undef": 1,
        "no-shadow": 0,
        "no-unused-vars": 0,
        "react/jsx-space-before-closing": 0,
        "no-underscore-dangle": 0,
        "arrow-body-style": 0,
        "no-restricted-syntax": 0,
        "jsx-a11y/img-has-alt": 0
    }
};