# Sotong Chrome Extension #


## start

``` shell
sudo npm install
sudo npm run build
```

## using webpack hot loader

``` shell 
sudo npm run watch
```

## react template reference 

http://preview.themeforest.net/item/react-admin/full_screen_preview/17647920?_ga=1.51130026.2089130481.1484530564

## extension on chrome browser

![extension](https://lh3.googleusercontent.com/-RPAjUddvKSI/WKFMxljNpkI/AAAAAAAAGA8/v3esyrWU43kp9Wbzj_IoxL34uyfHWRwRACL0B/w1060-d-h504-p-rw/%25E1%2584%2589%25E1%2585%25B3%25E1%2584%258F%25E1%2585%25B3%25E1%2584%2585%25E1%2585%25B5%25E1%2586%25AB%25E1%2584%2589%25E1%2585%25A3%25E1%2586%25BA%2B2017-02-13%2B%25E1%2584%258B%25E1%2585%25A9%25E1%2584%2592%25E1%2585%25AE%2B3.04.29.png)

